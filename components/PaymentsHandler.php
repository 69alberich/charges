<?php namespace QchSoft\Charges\Components;

use Cms\Classes\ComponentBase;
use Lovata\Toolbox\Classes\Helper\UserHelper;
use Lovata\OrdersShopaholic\Models\Order;
use QchSoft\Charges\Models\Payment;
use QchSoft\Charges\Models\Settings;
use Input;
use Response;
use Validator;
use Flash;
use Mail;

class PaymentsHandler extends ComponentBase{
    
    protected $obUser;
    protected $obOrder;

    public function componentDetails()
    {
        return [
            'name'        => 'Payments Handler',
            'description' => 'test',
        ];
    }

    public function defineProperties()
    {
        return [
            'code' => [
                'title' => 'Code',
                'description' => 'Code used to find order',
                'type' => 'string',
                'required' => 'true',

            ],
            'type' => [
                "title" => 'Type',
                'description' => 'find by code or slug',
                'type' => "dropdown",
                'options'     => ['number'=>'by number', 'slug'=>'by slug']
            ]
        ];
    }
    public function init(){
        //$this->obUser = UserHelper::instance()->getUser();
        if ($this->property('type') == "slug") {
            $this->obOrder =  Order::getBySecretKey($this->property('code'))->first();
        }else{
            $this->obOrder =  Order::getByNumber($this->property('code'))->first();
        }
        
    }
    /*
    
    $data["archivo"] = Input::file("archivo2");
      //VALIDACIONES
      // 1 tipo de datos
      $rules = [
        "localizador" => "min:4|max:20|required",
        "archivo" => "mimes:jpeg,pdf,png|max:2048|required"
      ];

      $messages = [
          'required' => ' :attribute es requerido.',
          'mimes' => 'Sólo son permitidos archivos en formato JPG o PDF',
          //'archivo.max' => 'El peso máximo permitido es 2MB'
      ];

    */
    public function onAddPayment(){
        $data = post();
        $file = $data["file"] = Input::file("capture");
        $validatorMessage ="";

        if($data["method_id"]==2){
            $validator = Validator::make(
            [
               // 'referencia' => $data["reference"],
                'comprobante' => $data["file"],
            ],
            [
                //'referencia' => 'required',
                'comprobante' => 'mimes:jpeg,pdf,png|max:2048|required',
            ],
            [
                'required' => ' :attribute es requerido.',
                'mimes' => 'Sólo son permitidos archivos en formato JPG o PDF',
            ]
            );
            if ($validator->fails()) {
                $messages = $validator->messages();
                
                foreach ($messages->all('<li>:message</li>') as $message) {
                    $validatorMessage .= $message;
                }
    
                return Flash::error($validatorMessage);
            }
        }
        
        $payment = new Payment();
        $payment->order_id = $this->obOrder->id;
        $payment->mount = $this->obOrder->total_price;
        if(isset($data["reference"])){
            $payment->reference = $data["reference"];
        }
        $payment->payment_method_id = $data["method_id"];
        $payment->currency = 1;
        $payment->payment_status_id = $data["status_id"];
        
        $payment->usable_id = 0;
        $payment->usable_type = '';

        if($file != null ){
            $payment->file = $file;
        }
        $this->obOrder->status_id = $data["order_status_id"];
        $this->obOrder->payment_method_id = $data["method_id"];
        
        if($payment->save() && $this->obOrder->save()){
            // These variables are available inside the message as Twig
            $property = $this->obOrder->manager->properties[0];
            $vars = ['order' => $this->obOrder, 'property' => $property];


            $emailList = Settings::get('email_list');

            if($emailList !== '') {
                $mail = Mail::send('qchSoft.charges::mail.payment_received ', $vars, function($message) use($property) {
                    $emailList = explode(',',Settings::get('email_list'));
                    $propertyEmailList = explode(',',$property->email_notification);
                    if($property->email_notification !== ''){
                        foreach($propertyEmailList as $emailSender){
                            $message->to(trim($emailSender), 'Cliente');
                        }
                        foreach($emailList as $emailSender){
                            $message->bcc(trim($emailSender), 'Cliente');
                        }
                    } else {
                        foreach($emailList as $emailSender){
                            $message->to(trim($emailSender), 'Cliente');
                        }
                    }
                });
            }
            return Response::json(['success' => true, 'payment_id' => $payment->id]);
        }else{
            return Response::json(['success' => false]);
        }
    }
}