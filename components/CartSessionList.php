<?php namespace QchSoft\Charges\Components;

use QchSoft\Charges\Classes\Processor\CustomOrderProcessor;
use Lovata\OrdersShopaholic\Components\MakeOrder;
use Lovata\Toolbox\Classes\Helper\UserHelper;

use Input;
use Session;
use Validator;
use Flash;
use Response;
class CartSessionList extends MakeOrder
{
    
    /** @var \Lovata\Buddies\Models\User */
    protected $obUser;

    public function componentDetails()
    {
        return [
            'name'        => 'Cart Session List',
            'description' => 'List of Charges in Session',
        ];
    }


    public function onAddCharge(){

        $data = Input::get();
        $session = Session::all();
        $validator = Validator::make(
          [
          'price' => $data["price"],
          'title' => $data["title"],
          ],
          [
            'price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'title' => 'required',
          ],
          ['price.regex' => "Formato de precio incorrecto",
          'title.required' => 'Titulo es requerido',
          'price.required' => 'Monto es requerido',
          ]
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            $retorno = "";
            foreach ($messages->all('<li>:message</li>') as $message) {
              $retorno .= $message;
            }
            return  Flash::error($retorno);
        }else{

         if (isset($session["charges"]) && $session["charges"]!=null) {
            $array = $session["charges"];
            array_push($array, $data);
              Session::put('charges', $array);
            
          }else{
            $array = array("0" => $data);
            Session::put("charges", $array);
          }
           //Flash::success("Ok");
          
        }
      }
    public function onRemoveCharge(){
      $data = Input::get();
      $session = Session::all();

      if (isset($session["charges"]) && $session["charges"]!=null) {
        $current_array = $session["charges"];
      
        unset($current_array[$data["index"]]);
        $new_array = array();
        foreach ($current_array as $key => $value) {
          array_push($new_array,$value);
        }
        Session::put('charges', $new_array);
      }
    }

    public function getSessionCartList(){
      $session = Session::all();
      if(isset($session["charges"])){
        return $session["charges"];
      }else{
        return null;
      }
      
    }

    public function onCreateOrder(){
      $session = Session::all();
      $user = UserHelper::instance()->getUser();
      $data = post();
      //trace_log($data["charges"]);
      $charges = null;

      $messageCharges = "";

      if(isset($session["charges"]) && count($session["charges"]) > 0 ){
        $charges = $session["charges"];
      }elseif(isset($data["charges"]) && count($data["charges"]) > 0){
        $charges = $data["charges"];
      }

      $validator = Validator::make(
        [
        'name' => $data["name"],
        'lastname' => $data["lastname"],
        'email' => $data["email"],
        'document' => $data["document"],
        ],
        [
          'name' => 'required',
          'lastname' => 'required',
          'email' => 'required|email',
          'document' => 'required',
        ],
        ['name.required' => "Nombre es requerido",
          'lastname.required' => 'Apellido es requerido',
          'email.required' => 'Email es requerido',
          'email.email' => 'Formato de email inválido',
          'document.required' => 'Documento de identidad es requerido',
          ]
      );

      $validatorMessage = "";

      if(!$charges){
        $validatorMessage.="*Debes crear al menos 1 cargo<br>";
      }

      if ($validator->fails()) {
        $messages = $validator->messages();
       
        foreach ($messages->all('<li>:message</li>') as $message) {
          $validatorMessage .= $message;
        }
      }
      
      if($user == null){
        $validatorMessage="*Tu sesión ha caducado, refresca esta página e inicia sesión";
      }
     
      if ($validatorMessage != "") {
        return $validatorMessage;
      }

      $obOrder = CustomOrderProcessor::instance()->createOrder($charges, $data, $user);
      //Session::forget("charges");
      
      /*
      trace_log(Response::json(['success' => true, 
        'order_id' => $obOrder->id,
        'order_number' => $obOrder->order_number,
        'secret_key' =>$obOrder->secret_key,
      ]));
      */
      if($obOrder != null){
        Session::forget("charges");
        return ['success' => true, 
         'order_id' => $obOrder->id,
         'order_number' => $obOrder->order_number,
         'secret_key' =>$obOrder->secret_key,
         ];
      }else{
          return['success' => false];
      }

    }
}
