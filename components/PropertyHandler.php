<?php namespace QchSoft\Charges\Components;

use Cms\Classes\ComponentBase;
use QchSoft\Charges\Models\Property;
use Input;
use Lovata\Toolbox\Classes\Helper\UserHelper;
use QchSoft\Charges\Api\Items\PropertyItem;
class PropertyHandler extends ComponentBase{
    
    protected $obUser;
    

    public function componentDetails()
    {
        return [
            'name'        => 'Property Handler',
            'description' => 'method for properties/clients',
        ];
    }

    public function defineProperties()
    {
        return [];
    }
    
    public function getMainProperty(){

        $obUser = UserHelper::instance()->getUser();

        if($obUser == null)
        return null;

        $obProperty = $obUser->getFirstProperty();
        
        return PropertyItem::make(null, $obProperty);
    }
    
}