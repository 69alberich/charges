<?php namespace QchSoft\Charges\Components;

use Cms\Classes\ComponentBase;
use Lovata\Toolbox\Classes\Helper\UserHelper;

use Lovata\OrdersShopaholic\Models\Order;
use Lovata\OrdersShopaholic\Classes\Collection\OrderCollection;
use October\Rain\Support\Collection;
use Input;
use Response;
use Validator;
use Flash;

use QchSoft\Charges\Api\Items\OrderItem;

class OrdersHandler extends ComponentBase{

    public function componentDetails()
    {
        return [
            'name'        => 'Orders Handler',
            'description' => 'methods for orders',
        ];
    }

    public function getOrderList($filters = null){
        //trace_log($filters);

        $user = UserHelper::instance()->getUser();
        $arUsersId = array();
        if(!isset($user->properties[0])){
            return;
        }
        foreach ($user->properties[0]->users as $userItem) {
            $id = $userItem->id;
            array_push($arUsersId, $id);
        }
        $arStatuses = [];

        $orderList = Order::managers($arUsersId);
        if(isset($filters["statuses"])){
            //trace_log("entro acá");
            
            $arStatuses = explode(",", $filters["statuses"]);

            $orderList->status($arStatuses);
         
        }
        $result = $orderList->orderBy('created_at', 'desc');
        
        return $result->get();
    }


    public function getOrderItemList($filters = null){
        $user = UserHelper::instance()->getUser();

    
        $arUsersId = array();
        if(!isset($user->properties[0])){
            return;
        }

        foreach ($user->properties[0]->users as $userItem) {
            $id = $userItem->id;
            array_push($arUsersId, $id);
        }

        $arStatuses=null;

        if(isset($filters["statuses"]) && $filters["statuses"] !=""){
            $arStatuses = explode(",", $filters["statuses"]);

        }
        
        $orderList = Order::managers($arUsersId)->when($arStatuses, function ($query, $arStatuses){
            return $query->status($arStatuses);
        })->orderBy('created_at', 'desc')->paginate(15, $filters["page"]);

        return $orderList;
        
        //return OrderItem::make($id, null);
    }


    public function loadOrder($id = null, $model = null){
        return OrderItem::make($id, $model);
    }
}