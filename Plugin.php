<?php namespace Qchsoft\Charges;

use System\Classes\PluginBase;
use Event;


use Qchsoft\Charges\Classes\Event\OrdersControllerHandler;
use Qchsoft\Charges\Classes\Event\OrderPositionModelHandler;
use Qchsoft\Charges\Classes\Event\OrderModelHandler;
use Qchsoft\Charges\Classes\Event\UserModelHandler;
use Qchsoft\Charges\Classes\Event\BackendListsExtend;
use Qchsoft\Charges\Classes\Event\UserItemExtend;

//use Qchsoft\ShopPlus\Classes\Event\OrderModelHandler;


class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'QchSoft\Charges\Components\CartSessionList' => 'CartSessionList',
            'QchSoft\Charges\Components\PaymentsHandler'   => 'PaymentsHandler',
            'QchSoft\Charges\Components\PaymentsList'   => 'PaymentsList',
            'QchSoft\Charges\Components\OrdersHandler'   => 'OrdersHandler',
            'QchSoft\Charges\Components\PropertyHandler'   => 'PropertyHandler',
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Mail notifications',
                'description' => '',
                'category'    => 'dictya settings',
                'icon'        => 'icon-cog',
                'class'       => 'Qchsoft\Charges\Models\Settings',
                'order'       => 500
            ]
        ];
    }
    
    public function boot(){
        $this->addEventListener();
    }



    protected function addEventListener(){
       
       Event::subscribe(OrdersControllerHandler::class);
       Event::subscribe(OrderPositionModelHandler::class);
       Event::subscribe(OrderModelHandler::class);
       Event::subscribe(UserModelHandler::class);
       Event::subscribe(BackendListsExtend::class);
       Event::subscribe(UserItemExtend::class);
    }

    public function registerSchedule($schedule)
    {
        $schedule->call(function () {

           $arOrders = \Lovata\OrdersShopaholic\Models\Order::where("status_id", 1)->get();
            //trace_log(count($arOrders));
            foreach ($arOrders as $obOrder) {
                if (isset($obOrder->property["vigency_value"]) && isset($obOrder->property["vigency_format"])) {
                    if(!$obOrder->isAvailableToPay()){
                        $obOrder->status_id = 5;
                        $obOrder->save();
                    }
                }else{
                    $obOrder->status_id = 5;
                    $obOrder->save();
                }
                
           }
           // \Db::table('recent_users')->delete();
        })->hourly();
    }
}
