<?php namespace QchSoft\Charges\Classes\Processor;

use Lovata\OrdersShopaholic\Models\Order;
use Lovata\OrdersShopaholic\Models\OrderPosition;
use QchSoft\Charges\Models\Charge;
use October\Rain\Support\Traits\Singleton;
use Lovata\Shopaholic\Classes\Helper\CurrencyHelper;
use Lovata\Toolbox\Classes\Helper\UserHelper;
use Lovata\Buddies\Models\User;
use Db;

class CustomOrderProcessor{

    use Singleton;

    protected $obOrder;
    protected $obUser;
    protected $obManager;
    protected $arOrderData;
    protected $arCharges;
    protected $flag = true;
    
    public function createOrder($arCharges, $arOrderData, $user){
        
        $this->fillManager($user);
        $this->fillOrderData($arOrderData);
        $this->fillUser($arOrderData);
        $this->fillCharges($arCharges);

        Db::connection()->enableQueryLog();

        Db::beginTransaction();

        $this->createOrderObject();

        $queries = Db::getQueryLog();

        //trace_log($this->obUser->name);

        Db::commit();

        return $this->obOrder;
    }

    public function createOrderObject(){

        //trace_log($this->arOrderData);
        //trace_log($this->arOrderData);
        
        $this->obOrder = Order::create($this->arOrderData);

        foreach ($this->arCharges as $key => $charge) {
            $obCharge = Charge::create($charge);

            try {
                $obOrderPosition = OrderPosition::create([
                    'item_id'=> $obCharge->id,
                    'order_id'=> $this->obOrder->id,
                    'item_type'=>Charge::class,
                    'price'=> $obCharge->price,
                    'quantity'=> 1
                ]);
                $this->obOrder->order_position()->add($obOrderPosition);
            } catch (\October\Rain\Database\ModelException $obException) {
                trace_log($obException->getErrors());
                //$this->processValidationError($obException);
                return false;
            }

        }
        $this->obOrder->user()->add($this->obUser);
        $this->obOrder->save();

        
    }

    public function fillUser(){
        

        $sEmail = $this->arOrderData["property"]['email'];

        $this->obUser = UserHelper::instance()->findUserByEmail($sEmail);
        if (empty($this->obUser)) {
            $userData["email"] = $this->arOrderData["property"]['email'];
            $userData["name"] =  $this->arOrderData["property"]['name'];
            $userData["last_name"] =  $this->arOrderData["property"]['lastname'];
            $userData["password"] =  "dictya.com";
            $userData["password_confirmation"] =  "dictya.com";

            $this->obUser = User::create($userData);
            
        }
    }
    /*public $fillable = [
        'email',
        'password',
        'password_change',
        'password_confirmation',
        'name',
        'last_name',
        'middle_name',
        'middle_name',
        'phone',
        'phone_list',
        'property',
    ];*/
    public function fillManager($user){
        
        $this->obManager = $user;
        $property = $this->obManager->properties->first();
        $this->arOrderData["manager_id"] = $this->obManager->id;
        $this->arOrderData["property_id"] = $property->id;
    }

    public function fillCharges($arCharges){
        $this->arCharges = $arCharges;
    }

    public function fillOrderData($arOrderData){
        //trace_log("orderdata", $arOrderData);
        $sCurrencyCode = CurrencyHelper::instance()->getActive();

        $this->arOrderData["status_id"] = 1;
        $this->arOrderData["currency_id"] = $sCurrencyCode->id;
        $this->arOrderData["property"] = $arOrderData;
       /* public $fillable = [
            'user_id',
            'status_id',
            'shipping_type_id',
            'payment_method_id',
            'shipping_price',
            'property',
            'currency_id',
            'shipping_tax_percent',
            'manager_id',
            'payment_data',
        ];*/
        //$this->arOrderData = $arOrderData;
    }
}