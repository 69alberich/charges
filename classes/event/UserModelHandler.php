<?php namespace QchSoft\Charges\Classes\Event;

use Lovata\Buddies\Models\User;
use Qchsoft\Charges\Models\Property;

class UserModelHandler {
    public function subscribe($obEvent){

        User::extend(function($model) {
            if (!$model instanceof User) {
               
                return;
            }

            $model->belongsToMany["properties"] = [
                Property::class,
                'table'   => 'qchsoft_charges_buddyusers_property',
                'key'=> 'user_id',
                'pivot' => ['default', "is_master"],
            ];

            $model->addDynamicMethod('getFirstProperty', function() use ($model) {
               
                //$obModel = $obItem->getObject();

                return $model->properties->first();
            });

            //$model->addCachedField(['properties']);
        });
    }
}