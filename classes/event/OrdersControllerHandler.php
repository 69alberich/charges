<?php namespace Qchsoft\Charges\Classes\Event;

use Lovata\OrdersShopaholic\Models\Order as OrderModel;
use Lovata\OrdersShopaholic\Controllers\Orders as OrdersController;
use Lovata\OrdersShopaholic\Classes\Event\Order\OrderModelHandler;
use Event;
//use Mail;
//use Flash;
//use Redirect;
class OrdersControllerHandler extends OrderModelHandler{

    public function subscribe($obEvent){

        OrdersController::extendFormFields(function ($form, $model, $context) {
            // Prevent extending of related form instead of the intended User form
            if (!$model instanceof OrderModel) {
               
                return;
            }
           /* pregunta si el modelo ha sido creado, si lo descomento no funciona en create 
            if (!$model->exists) {
              
                return;
            }
            */
            $form->removeTab("lovata.ordersshopaholic::lang.tab.tasks");
            $form->removeTab("lovata.ordersshopaholic::lang.tab.offers_info");
            $form->removeField("payment_method");
            $form->removeField("shipping_type");
            $form->removeField("full_shipping_price");
            $form->removeField("manager_id");
           
            $form->addTabFields([
                'charge' => [
                    'tab' => 'Charges',
                    'type'  => 'partial',
                    'path' => '$/qchsoft/charges/partials/_charge_order_relation.htm',
                    'context' => 'update'
                ],
                'payments' => [
                    'tab' => 'Payments',
                    'type'  => 'partial',
                    'path' => '$/qchsoft/charges/partials/_payments_order_relation.htm',
                    'context' => 'update'
                ]
            ]);
            
        });

        OrdersController::extend(function($controller) {
            /*$controller->addDynamicMethod('onSendPreOrder', function($id) use ($controller) {
                $obOrder = OrderModel::find($id);
                $this->obElement = $obOrder;
                $this->sendUserEmailAfterCreating();
              
            });*/

            /*$controller->addDynamicMethod('onApproveOrder', function($id) use ($controller) {
                $obOrder = OrderModel::find($id);;
                $this->obElement = $obOrder;
                
                //$this->obElement = $obOrder;
                if($obOrder->status_id == 2 || $obOrder->status_id == 5){
                    
                    foreach ($obOrder->order_position as $order_position) {
                        //trace_log($order_position->quantity);
                        $order_position->offer->quantity -= $order_position->quantity;
                        $order_position->offer->save();
                    }
                    $obOrder->status_id = 3;
                    $obOrder->save();

                    $this->sendUserEmailAfterCreating();
                    Flash::success("Proceso completado");
                }else{
                    Flash::error("No se puede aprobar esta orden, refresca esta pantalla");
                }
            });*/

            if (!isset($controller->relationConfig)) {
                $controller->addDynamicProperty('relationConfig');
            }
        
            // Splice in configuration safely
            $myConfigPath = '$/qchsoft/charges/config/order_charge_relation.yaml';
            $myPaymentConfigPath = '$/qchsoft/charges/config/order_payment_relation.yaml';
            $extensionList = '$/qchsoft/charges/config/order_custom_config_list.yaml';

            $controller->relationConfig = $controller->mergeConfig(
                $controller->relationConfig,
                $myConfigPath
            );

            $controller->relationConfig = $controller->mergeConfig(
                $controller->relationConfig,
                $myPaymentConfigPath
            );

            $controller->listConfig = $extensionList;
        });
        
        OrdersController::extendListColumns(function($list, $model){
            if (!$model instanceof OrderModel) {
                return;
            }
            $list->removeColumn("created_at");
            $list->removeColumn("updated_at");
  
            $list->addColumns([
                'created_at' => [
                    'label' => 'lovata.toolbox::lang.field.created_at',
                    'sortable' => true,
                    'invisible' => true,
                    'type' => "datetime",
                    'format' => "d-m-Y"
                ],
                'order_property' => [
                    'label' => 'Property name',
                    'relation'=> 'order_property',
                    'select' => 'name',
                    'sortable'=> 'false',
                ],
                'updated_at' => [
                    'label' => 'lovata.toolbox::lang.field.updated_at',
                    'sortable' => true,
                    'invisible' => true,
                    'type' => "datetime",
                    'format' => "d-m-Y"
                ],
            ]);
        });
    }
    
}