<?php namespace QchSoft\Charges\Classes\Event;

use Lovata\Buddies\Classes\Item\UserItem;

class UserItemExtend {


    public function subscribe(){
        UserItem::extend(function($obItem) {
     
            //$obItem->arExtendResult[] = "properties";
            //$obItem->arExtendResult[] = "$obItem";
             
            $obItem->addDynamicMethod('getFirstProperty', function() use ($obItem) {
               
                $obModel = $obItem->getObject();

                return $obModel->properties->first();
            });

            
       });
    }

}
