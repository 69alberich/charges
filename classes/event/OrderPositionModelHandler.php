<?php namespace QchSoft\Charges\Classes\Event;

use Lovata\Toolbox\Classes\Event\ModelHandler;
use Lovata\OrdersShopaholic\Models\OrderPosition as OrderPositionModel;
use QchSoft\Charges\Models\Charge;


class OrderPositionModelHandler extends ModelHandler{
    public function subscribe($obEvent){

        OrderPositionModel::extend(function($model) {

            if (!$model instanceof OrderPositionModel) {
               
                return;
            }

            $model->belongsTo["charge"] = [
                Charge::class,
                'key'   => 'item_id',
            ];
            
        });
    }

    /**
     * Get model class name
     * @return string
     */
    protected function getModelClass()
    {
        return Order::class;
    }

    /**
     * Get item class name
     * @return string
     */
    protected function getItemClass()
    {
        return OrderItem::class;
    }
}