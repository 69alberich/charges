<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftChargesPaymentTypes extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_charges_payment_types', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('code');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_charges_payment_types');
    }
}
