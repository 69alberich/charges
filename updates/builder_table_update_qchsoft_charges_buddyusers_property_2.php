<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftChargesBuddyusersProperty2 extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_charges_buddyusers_property', function($table)
        {
            $table->integer('is_master')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_charges_buddyusers_property', function($table)
        {
            $table->dropColumn('is_master');
        });
    }
}
