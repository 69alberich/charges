<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftChargesProperty extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_charges_property', function($table)
        {
            $table->string('stripe_public_key', 150);
            $table->string('stripe_private_key', 150);
    
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_charges_property', function($table)
        {
            $table->dropColumn('stripe_public_key');
            $table->dropColumn('stripe_private_key');
    
        });
    }
}
