<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftChargesPaymentsPaymentType extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_charges_payments', function($table)
        {
            $table->integer('payment_type_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_charges_payments', function($table)
            {
                $table->dropColumn('payment_type_id');    
            });
    }
}
