<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftChargesPayments extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_charges_payments', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('order_id');
            $table->decimal('mount', 15, 2);
            $table->string('reference', 250);
            $table->integer('payment_method_id');
            $table->integer('currency_id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('payment_status_id');
            $table->integer('usable_id');
            $table->string('usable_type', 250);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_charges_payments');
    }
}
