<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftChargesBuddyusersProperty extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_charges_buddyusers_property', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('property_id');
            $table->integer('user_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_charges_buddyusers_property');
    }
}
