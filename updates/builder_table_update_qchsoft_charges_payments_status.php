<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftChargesPaymentsStatus extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_charges_payments_status', function($table)
        {
            $table->string('color', 150)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_charges_payments_status', function($table)
        {
            $table->dropColumn('color');
        });
    }
}
