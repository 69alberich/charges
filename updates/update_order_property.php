<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Rain\Database\Schema\Blueprint;
use Lovata\OrdersShopaholic\Models\Order;

class UpdateOrderProperty extends Migration
{
    public function up()
    {
        $orders = Order::all();
        
        foreach($orders as $order){
            $manager = $order->manager;
            if ($manager != null) {
                $property = $manager->properties->first();

                if($property != null ) {
                    $order->property_id = $property->id;
                    $order->save();
                }
            }
            

            
        }
    }
    
    public function down()
    {
        
    }
}