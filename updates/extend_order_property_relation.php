<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Rain\Database\Schema\Blueprint;

class extend_order_property_relation extends Migration
{
    public function up()
    {
        Schema::table('lovata_orders_shopaholic_orders', function($table)
        {
            $table->integer('property_id')->unsigned()->nullable();
            $table->foreign('property_id')->references('id')->on('qchsoft_charges_property');
        });
    }
    
    public function down()
    {
        Schema::table('lovata_orders_shopaholic_orders', function($table)
        {
            $table->dropForeign('lovata_orders_shopaholic_orders_property_id_foreign');
            $table->dropColumn('property_id');    
        });
    }
}