<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftChargesProperty4 extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_charges_property', function($table)
        {
            $table->text('zelle_manager');
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_charges_property', function($table)
        {
            $table->dropColumn('zelle_manager');
        });
    }
}