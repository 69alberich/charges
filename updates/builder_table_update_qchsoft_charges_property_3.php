<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftChargesProperty3 extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_charges_property', function($table)
        {
            $table->text('email_notification');
            
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_charges_property', function($table)
        {
            $table->dropColumn('email_notification');
            
        });
    }
}
