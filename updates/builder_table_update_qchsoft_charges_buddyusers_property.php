<?php namespace Qchsoft\Charges\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftChargesBuddyusersProperty extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_charges_buddyusers_property', function($table)
        {
            $table->integer('default')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_charges_buddyusers_property', function($table)
        {
            $table->dropColumn('default');
        });
    }
}
