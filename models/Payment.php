<?php namespace Qchsoft\Charges\Models;

use Model;
use Qchsoft\Charges\Models\PaymentStatus;
use Qchsoft\Charges\Models\PaymentType;
use Lovata\Shopaholic\Models\Currency;
use Lovata\OrdersShopaholic\Models\Order;
use Lovata\OrdersShopaholic\Models\PaymentMethod;
/**
 * Model
 */
class Payment extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_charges_payments';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $filliable = [
        "order_id",
        "mount",
        "reference",
        "payment_method_id",
        "currency_id",
        "payment_type_id",
        "payment_status_id",
        "usable_id",
        "usable_type",
    ];

    public $belongsTo = [
        'status'         => [PaymentStatus::class, "key" => "payment_status_id"],
        'payment_type'   => [PaymentType::class, "key" => "payment_type_id"],
        'currency'       => [Currency::class],
        'order'       => [Order::class],
        'payment_method' => [PaymentMethod::class],
    ];

    public $attachOne = [
        'file'=> 'System\Models\File'
      ];

    
      public function scopeUser($query, $id){
          return $query->where("usable_id", $id);
      }

      /**
     * Get currency_symbol attribute value
     * @return null|string
     */
    protected function getCurrencySymbolAttribute()
    {
        //Get currency object
        $obCurrency = $this->currency;
        if (empty($obCurrency)) {
            return null;
        }

        return $obCurrency->symbol;
    }
}
