<?php namespace Qchsoft\Charges\Models;
use Lovata\Buddies\Models\User;
use Lovata\OrdersShopaholic\Models\PaymentMethod;

use Model;

/**
 * Model
 */
class Property extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_charges_property';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsToMany =[
        'users' => [
          User::class,
          'table' => 'qchsoft_charges_buddyusers_property',
          'pivot' => ['default', "is_master"],
        ],
        'payment_methods' => [
          PaymentMethod::class,
          'table' => 'qchsoft_charges_property_payments_methods'
        ],
      ];
    public $attachOne = [
      'logo' => 'System\Models\File',
    ];

    public $attachMany = ['images' => 'System\Models\File'];
}
