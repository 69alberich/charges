<?php namespace Qchsoft\Charges\Models;

use Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'qchsoft_charges_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}