<?php namespace QchSoft\Charges\Api\Items;
use Lovata\OrdersShopaholic\Models\Order;

class OrderItem {

    public static function make($orderId, $obOrder = null){

        $public_fields = ["id", "order_number", "total_price", "status_id", "secret_key", "document"];
        $relations = [
            "payment_method" => [
                "name"
            ],

           
        ];

        if ($orderId) {
            $obOrder = Order::find($orderId);
        }else{
            $obOrder = $obOrder;
        }
        
        $element = array();
        //SET PROPERTIES
        if ($obOrder != null) {
           foreach ($obOrder->attributes as $key => $value) {
            if (in_array($key, $public_fields)) {
                $element[$key] = $value; 
            }
            
           }

           //SET RELATIONS
        /*
        foreach ($relations as $key => $value) { //RECORRO EL ARRAY DE RELACIONES
            if(($relationElement = $obOrder->{$key}) !== null ){ // si el key existe en las relaciones del objeto

               foreach ($relationElement as $relationKey => $relationValue) {
                    if(is_object($relationValue)){
                        foreach ($relationValue->attributes as $attributeKey => $attributeValue) {
                            //trace_log($attributeKey);
                            if (in_array($attributeKey, $relations[$key])) {
                                //trace_log("entre");
                                $element[$key][$relationKey][$attributeKey] = $attributeValue;
                            }
                        } 
                    }
                       
               }               
            }
        }
        */
        $element["user"] = $obOrder->user;
        $element["created_at"] = $obOrder->created_at->format("d/m/Y");
        $element["status_name"] = $obOrder->status->name;
        $element["status_color"] = $obOrder->status->color;
        $element["currency_code"] = $obOrder->currency->code;                
        $element["properties"] = $obOrder->property;
        $element["number"] = $obOrder->order_number;
        $element["total"] = $obOrder->total_price;
        
        $charges = array();

        foreach ($obOrder->order_position as $obOrderPosition ) {

            $charge["title"] = $obOrderPosition->charge->title;
            $charge["description"] = $obOrderPosition->charge->description;
            $charge["quantity"] = $obOrderPosition->charge->quantity;
            $charge["price"] = $obOrderPosition->total_price;
            $charge["currency_code"] = $element["currency_code"];

            array_push($charges, $charge);
        }
        
        $element["charges"] = $charges;

        $payments = array();

        foreach ($obOrder->payments as $obPayment ) {

            $payment["id"] = $obPayment->id;
            $payment["amount"] = $obPayment->mount;
            $payment["reference"] = $obPayment->reference;
            $payment["currency"] = $obPayment->currency->code;
            $payment["created_at"] = $obPayment->created_at->format("d/m/Y");
            $payment["status_id"] = $obPayment->payment_status_id;
            $payment["status_name"] = $obPayment->status->name;
            $payment["payment_method"] = $obPayment->payment_method->name;
            $payment["order_number"] = $obOrder->order_number;
            $payment["order_id"] = $obOrder->id;
            $payment["status_color"] = $obPayment->status->color;

            array_push($payments, $payment);
        }
        $element["payments"] = $payments;
        //media

        //$element["logo"] = $obOrder->logo->path;

        //pivot

        //$element["default"] = $obProperty->pivot->default;
        //$element["is_master"] = $obProperty->pivot->is_master;
        return $element;
        }else{
            return null;
        }
    }

}
