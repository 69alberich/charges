<?php namespace QchSoft\Charges\Api\Items;
use QchSoft\Charges\Models\Property;

class PropertyItem {

    public static function make($propertyId, $obProperty = null){

        $public_fields = ["name", "description", "slug", "preview_text", "active", "document"];

        $relations = array(

            "payment_methods" => [
                "id", "name", "preview", "active", "code"
            ]
            
        );

        if ($propertyId) {
            $obProperty = Property::find($propertyId);
        }else{
            $obProperty = $obProperty;
        }
        
        $element = array();
        //SET PROPERTIES
        if ($obProperty != null) {
           foreach ($obProperty->attributes as $key => $value) {
            if (in_array($key, $public_fields)) {
                $element[$key] = $value; 
            }
            
           }
        }
        //SET RELATIONS
        foreach ($relations as $key => $value) { //RECORRO EL ARRAY DE RELACIONES
            
            if($relationElement = $obProperty->{$key}){ // si el key existe en las relaciones del objeto
               
               foreach ($relationElement as $relationKey => $relationValue) {
                    
                    foreach ($relationValue->attributes as $attributeKey => $attributeValue) {
                        //trace_log($attributeKey);
                        if (in_array($attributeKey, $relations[$key])) {
                            //trace_log("entre");
                            $element[$key][$relationKey][$attributeKey] = $attributeValue;
                        }
                    }    
               }               
            }
        }
        //media
        $element["logo"] = $obProperty->logo->path;
        //pivot
        $element["default"] = $obProperty->pivot->default;
        $element["is_master"] = $obProperty->pivot->is_master;

        return $element;
    }

}
